<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This is unofficial client for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thanks to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Russian SailfishOS Community channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sources: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>in Telegram for their help.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Using Emoji from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Emoji categories icon design - </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> from Junnxy studio (links below):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thanks to users (contribute and testing):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you want to donate, you can do that by:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>url: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>status: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add account ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loggining</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChannelLabel</name>
    <message>
        <source>Public channes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Private channes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direct channes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Typing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChannelsPage</name>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Offline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>server custom name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>server address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>use authentication token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authentication token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username or Email address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Certificate options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>trust certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CA certificate path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose server certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server certificate path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MattermostQt</name>
    <message>
        <source>Cant open CA certificate file: &quot;%0&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cant open certificate file: &quot;%0&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File: id(%0); </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageEditorBar</name>
    <message>
        <source>Message...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uploading </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessagesPage</name>
    <message>
        <source>get older</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add reaction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsPage</name>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show blobs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show blobs unders messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blobs opacity value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Markdown (beta)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use markdown formated text in messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page padding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cache size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send message icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mail Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reaction size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Channels view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show search field in channels view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Messages view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable send photo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable send Photo in attach menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use logging in to standart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Log level (to stderr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use logging</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ParentMessageItem</name>
    <message>
        <source>Reply to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit message from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Answer to message from</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Login failed because of invalid password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%0 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%0 Kb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%0 Mb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(you)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>somebody</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>(edited)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>KB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReactionsPage</name>
    <message>
        <source>Reactions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TeamOptions</name>
    <message>
        <source>Team Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TeamsPage</name>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
